#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
import cgi, sys
import cgitb; cgitb.enable()

conn = sqlite3.connect('cgi-bin/user.db')
curs = conn.cursor()
form = cgi.FieldStorage()

print("Content-type: text/html")
html = """
	<!DOCTYPE html>
	<html lang="pt-br">
	  <head>
	    <meta charset="utf-8">
	    <title>Home | Delta</title>
		<link rel="stylesheet" href="../css/style.css">
		<link rel="shortcut icon" href="../imagens/favicon.ico" type="image/x-icon">
	  </head>
	<body>
	<div id="menu">
				<nav>
			 		<ul>
			 		<!--menus da parte superior do layout-->
			 			<li><a href="#">Home</a></li>
			 			<li><a href="#">Sobre</a></li>
			 			<li><a href="users.py">Gerenciador de Usuarios</a></li>
			 			<li><a href="#">Cursos</a></li>
			 			<li><a href="../html/cadastro.html">Cadastro</a></li>
			 			<li><a href="../html/login.html">Login</a></li>
			 			<li><a href="posta.py">Gerenciador de Posts</a></li>
			
			 		</ul>
				</nav>
		 </div>
		 <body>
		 <div class="conteudo">
"""
print(html)